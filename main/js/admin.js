(function($){	
	var getSaveData = function(){
		return {
			"initialized": true,
			"kiosk_url": $("#kiosk_url").val(),
			"username": $("#username").val(),
			"password": $("#password").val(),
			"inactivity_reset": {"enabled" : $("#inactivity_reset").prop("checked"), "timeout": parseInt($("#inactivity_reset_timeout").val())},
			"daily_restart": {"enabled": $("#daily_restart").prop("checked"), "time": $("#daily_restart_time").val()},
			"context_menu": {"disabled": $("#context_menu_disabled").prop("checked")}
		};
	}
	
	var loadInactivityResetSettings = function(data){
		if ('inactivity_reset' in data){
			$("#inactivity_reset").prop("checked", data.inactivity_reset.enabled);
			$("#inactivity_reset_timeout").val(data.inactivity_reset.timeout);
			
			if (data.inactivity_reset.enabled)
				$(".inactivity-timeout-group").show();
			else
				$(".inactivity-timeout-group").hide();
		}else{
			$("#inactivity_reset").prop("checked", false);
			$("#inactivity_reset_timeout").val("5");
			$(".inactivity-timeout-group").hide();
		}
	}
	
	var loadDailyRestartSettings = function(data){
		if ('daily_restart' in data){
			$("#daily_restart").prop("checked", data.daily_restart.enabled);
			$("#daily_restart_time").timepicker({"defaultTime": data.daily_restart.time});
			
			if (data.daily_restart.enabled)
				$(".daily-restart-group").show();
			else
				$(".daily-restart-group").hide();
		}else{
			$("#daily_restart").prop("checked", false);
			$("#daily_restart_time").timepicker({"defaultTime": "12:00 AM"});
			$(".daily-restart-group").hide();
		}
	}
	
	var loadContextMenuSettings = function(data){
		if ('context_menu' in data)
			$("#context_menu_disabled").prop("checked", data.context_menu.disabled);
		else
			$("#context_menu_disabled").prop("checked", false);
	}
	
	var loadSettings = function(){
		chrome.storage.local.get(null, function(data){
			$("#kiosk_url").val(data.kiosk_url);
			$("#username").val(data.username);
			$("#password").val(data.password);
			loadInactivityResetSettings(data);
			loadDailyRestartSettings(data);
			loadContextMenuSettings(data);
		});
	}
	
	var saveSettings = function(){
		$(".form-group").removeClass("has-error");
		
		var saveData = getSaveData();
		var errs = 0;
		
		if (!saveData.kiosk_url){
			$("#kiosk_url").closest(".form-group").addClass("has-error");
			errs++;
		}
		
		if (!saveData.username){
			$("#username").closest(".form-group").addClass("has-error");
			errs++;
		}
		
		if (!saveData.password){
			$("#password").closest(".form-group").addClass("has-error");
			errs++;
		}
		
		if (saveData.inactivity_reset.enabled && (isNaN(saveData.inactivity_reset.timeout))){
			$(".inactivity-timeout-group").addClass("has-error");
			errs++;
		}
		
		if (errs == 0)
			chrome.storage.local.set(saveData);
	}
	
	$(document).ready(function(){
		loadSettings();
	}).on("click", ".exit-button", function(e){
		chrome.app.window.current().close();
	}).on("click", ".reset-button", function(e){
		chrome.storage.local.clear();
		loadSettings();
	}).on("click", ".save-button", function(e){
		saveSettings();
	}).on("click", ".launch-button", function(e){
		chrome.storage.local.get(null, function(data){
			if ('initialized' in data){
				if (data.kiosk_url && data.username && data.password)
					utility.showKiosk();
			}
		});
	}).on("change", "#inactivity_reset", function(e){
		var chk = $(this);
		if (chk.prop("checked"))
			$(".inactivity-timeout-group").show();
		else
			$(".inactivity-timeout-group").hide();
	}).on("change", "#daily_restart", function(e){
		var chk = $(this);
		if (chk.prop("checked"))
			$(".daily-restart-group").show();
		else
			$(".daily-restart-group").hide();
	}).on("keydown", function(e){
		if (e.keyCode == 13){
			e.preventDefault();
			saveSettings();
		}
	});
}(jQuery));