$(function(){	
	chrome.storage.local.get(null, function(data){		
		var toggleConsole = function(){
			if ($(".console").is(":visible")){
				$("#map").css("height", "100%");
				$(".console").hide();
			}else{
				$("#map").css("height", "85%");
				$(".console").css("height", "15%").show();
				$("#console-input").focus();
			}
		}
		
		var consoleAppend = function(msg){
			$(".console-output").prepend(
				$("<div/>").append("["+moment().format("YYYY-MM-DD HH:mm:ss")+"] ").append(msg)
			);
		}
		
		var login = function(){
			var un = $("#un").val();
			var pw = $("#pw").val();
			
			if (un && pw){
				return un == data.username && pw == data.password;
			}else{
				return false;
			}
		}
		
		var loginAction = function(){
			$(".error").hide();
			utility.showAdmin($("#un").val(), $("#pw").val(), function(){
				$(".error").show();
			});
		}
		
		var exitAction = function(){
			$(".error").hide();
			if (login())
				chrome.app.window.current().close();
			else
				$(".error").show();
		}
		
		var consoleAction = function(){
			var cmd = $("#console-input").val();
			$("#console-input").val("");
			
			switch(cmd){
				case "exit " + data.username + ":" + data.password:
					chrome.app.window.current().close();
					break;
				case "settings":
					var settings = $.extend(true, {}, data);
					settings.username = "*****";
					settings.password = "*****";
					$(".console-output").prepend($("<div/>").append(JSON.stringify(settings)));
					break;
			}
		}
		
		var kioskReset = function(){
			var src = $("#map").attr("src");
			if (src != data.kiosk_url)
				$("#map").attr("src", data.kiosk_url);
		}
		
		var activeTimeout = null;
		
		var active = function(){
			if (activeTimeout) clearTimeout(activeTimeout);
			activeTimeout = setTimeout(function(){
				consoleAppend("kiosk reset due to inactivity");
				kioskReset();
			}, data.inactivity_reset.timeout * 60 * 1000);
		}
		
		//prevent existing fullscreen on escape key press
		$(window).on("keydown keyup", function(e){ 
			if (e.keyCode == 27){
				e.preventDefault();
			}else if (e.keyCode == 65 && e.ctrlKey && e.shiftKey){
				$("#login").modal({"show": true});
			}else if (e.keyCode == 67 && e.ctrlKey && e.shiftKey){
				toggleConsole();
			}
		});
		
		$(document).on("click", ".kiosk-admin-login", function(e){
			loginAction();
		}).on("click", ".kiosk-admin-exit", function(e){
			exitAction();
		}).on("keydown", "#un,#pw", function(e){
			if (e.keyCode == 13){
				e.preventDefault();
				loginAction();
			}
		}).on("keydown", "#console-input", function(e){
			if (e.keyCode == 13){
				e.preventDefault();
				consoleAction();
			}
		}).on("contentload", "#map", function(e){
			consoleAppend("contentload: " + $("#map").attr("src"));
			if (data.context_menu.disabled){
				e.target.executeScript({"code": "window.oncontextmenu = function(){return false;};"});
			}
		}).on("shown.bs.modal", "#login", function(e){
			$('#un').focus();
		}).ready(function(){
			$("#map").attr("src", data.kiosk_url);
			if (data.inactivity_reset.enabled){
				$("*").on("click mousedown mouseup mousemove touch touchstart touchend keypress keydown", active);
				active();
			}
		});
		
		var restartTimeout = null;
		
		if (data.daily_restart.enabled){
			
			var now = moment();
			
			// get today midnight AM
			var m = moment(now.format("YYYY-MM-DD"));
			
			// inital expire is midnight plus the time component
			var expire = moment(m.format("YYYY-MM-DD") + " " + data.daily_restart.time, "YYYY-MM-DD h:mm:ss");
			
			// if already expired move expire to next day
			if (now.isAfter(expire))
				expire.add(1, 'days');
			
			// the difference is the ms between now and the exiration
			var diff = expire.diff(now);
			
			if (restartTimeout) clearTimeout(restartTimeout);
			
			restartTimeout = setTimeout(utility.showKiosk, diff);
			
			consoleAppend("daily reset will occur at " + now.clone().add(diff, 'ms').format("YYYY-MM-DD HH:mm:ss"));
		}
	});	
});