var utility = {
	showKiosk: function(){
		chrome.app.window.create("main/application.html",{
			"id": "kiosk",
			"state": "fullscreen"
		}, function(w){
			chrome.app.window.current().close();
			w.fullscreen();
		});
	},
	showAdmin: function(un, pw, callback){
		chrome.storage.local.get(null, function(data){
			
			var auth = true;
			
			if ('initialized' in data)
				auth = un == data.username && pw == data.password;
			
			if (auth){
				chrome.app.window.create("main/admin.html", {
					"id": "admin",
					"state": "fullscreen"
				}, function(w){
					chrome.app.window.current().close();
					w.fullscreen();
				});
			}else{
				callback();
			}
		});		
	}
}