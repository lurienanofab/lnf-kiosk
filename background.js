chrome.app.runtime.onLaunched.addListener(function(){
	chrome.storage.local.get(null, function(data){
		if ('initialized' in data)
			utility.showKiosk();
		else
			utility.showAdmin();
	});
});