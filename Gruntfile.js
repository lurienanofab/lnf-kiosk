module.exports = function (grunt) {
    grunt.initConfig({
        bowercopy: {
            options: {
                destPrefix: 'lib'
            },
            jquery: {
                files: {
                    'jquery/jquery.min.js': 'jquery/dist/jquery.min.js'
                }
            },
            bootstrap: {
                files: {
                    'bootstrap/js/bootstrap.min.js': 'bootstrap/dist/js/bootstrap.min.js',
                    'bootstrap/css/bootstrap.min.css': 'bootstrap/dist/css/bootstrap.min.css',
                    'bootstrap/fonts': 'bootstrap/dist/fonts/*',
					'bootstrap/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js': 'bootstrap-timepicker/js/bootstrap-timepicker.js'
                }
            },
            moment: {
                files: {
                    'moment/moment.min.js': 'moment/min/moment.min.js'
                }
            }
        },
		less: {
			development: {
				files: {
					'lib/bootstrap/plugins/bootstrap-timepicker/css/timepicker.css': 'bower_components/bootstrap-timepicker/css/timepicker.less'
				}
			}
		},
		copy: {
			main: {
				files: [
					{expand: true, src: ['lib/**'], dest: 'dist/lnf-kiosk/'},
					{expand: true, src: ['main/**'], dest: 'dist/lnf-kiosk/'},
					{expand: true, src: ['background.js', 'utility.js', 'manifest.json'], dest: 'dist/lnf-kiosk/'}
				]
			}
		},
		compress: {
			main: {
				options: {
					archive: 'dist/lnf-kiosk.zip',
					mode: 'zip'
				},
				files: [
					{expand: true, cwd: 'dist/lnf-kiosk', src: ['lib/**'], dest: 'lnf-kiosk/'},
					{expand: true, cwd: 'dist/lnf-kiosk', src: ['main/**'], dest: 'lnf-kiosk/'},
					{expand: true, cwd: 'dist/lnf-kiosk', src: ['background.js', 'utility.js', 'manifest.json'], dest: 'lnf-kiosk/'},
				]
			}
		}
    });

    grunt.loadNpmTasks('grunt-bowercopy');
	grunt.loadNpmTasks('grunt-contrib-less');
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-compress');
	
	grunt.registerTask('default', ['bowercopy', 'less', 'copy', 'compress']);
};